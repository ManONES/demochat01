package com.manycode.app.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootWebsocket3Application {

	@GetMapping("/")
	public String welcome() {
		return "Hola al mundo de Many  con  PUSH AUTOMATICO!";
	}	

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebsocket3Application.class, args);
	}

}
